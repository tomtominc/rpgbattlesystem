﻿
using UnityEngine;

[System.Serializable]
public class ElementData
{
    public Element element;
    public Sprite icon;
    public Color color;
    public Element weakTo;
    public Element strongTo;
}
