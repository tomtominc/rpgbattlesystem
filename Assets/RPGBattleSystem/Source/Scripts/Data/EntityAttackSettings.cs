﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EntityAttackSettings
{
    public string animation;
    public Transform attackSpawnLocation;
    public AudioClip attackStartSound;
}
