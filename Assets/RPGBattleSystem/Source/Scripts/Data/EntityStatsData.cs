﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EntityStatsData : ScriptableObject
{
    public int level;
    public int health;
    public int power;
    public int defense;
    public int mana;
    public int resistence;
    public int speed;
}
