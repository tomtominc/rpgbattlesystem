﻿

public enum AttackMoveType
{
    NONE,
    SPAWN_ON_TARGET,
    PROJECTILE,
}
