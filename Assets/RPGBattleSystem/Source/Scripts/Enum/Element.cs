﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Element
{
    METAL,
    FIRE,
    ICE,
    EARTH,
    ELECTRIC
}
