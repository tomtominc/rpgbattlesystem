﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HitTarget
{
    SINGLE,
    SELF,
    RANDOM_ALL,
    RANDOM_ENEMIES,
    RANDOM_FRIENDLY
}