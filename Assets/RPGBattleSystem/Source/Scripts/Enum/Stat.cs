﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Stat
{
    HEALTH,
    POWER,
    MAGIC,
    DEFENSE,
    RESISTENCE,
    SPEED,
}
