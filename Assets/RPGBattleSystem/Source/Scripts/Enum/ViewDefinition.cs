﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ViewDefinition
{
    BATTLE_OVERLAY_MENU,
    BATTLE_END_MENU,
    CHARACTER_SELECT_SCREEN
}
