﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleEndState : BattleState
{
    public BattleEndState(ApplicationController p_controller)
        : base(p_controller) { }
}
