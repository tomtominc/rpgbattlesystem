![](https://lh6.googleusercontent.com/dw8u2-OctCYHM9Jcp_qduarSEU055Hn97hQZgTYAWkA5lPoda55mfcygNNg2qDKAKjLfcIzAYf14ApVIWhzu7waY-Od9P9DiO5u0dS9ukij9dLY-gp-2aRswJtx68yryorxWi5zd)

**RPG Battle System** is an easy to learn turn based battle system that can plug into any RPG Game or be the starting point for your new RPG Game. ⚔️  
  
**Requires DOTween or DOTween Pro to be imported and setup in the project before importing the RPG Battle System package.**  
  
⚡️**FEATURES**⚡️  

⚡️  **Character Creation:**  Simple character creation done with native objects & includes many options like,  **Portrait**,  **Element**,  **Attacks**& More!  
  
⚡️  **Attack Creation:**  Simple attack creation done with native objects & includes options like,  **Cost**,  **Element**,  **Attack Type**  (Melee, Ranged, Stat Boosting) & More!  
  
⚡️  **Environment Creation:**  Environments can be swapped out depending on the where you want the battle to take place and offers options like,  **Background Image**,  **Background Music**, and other various things.  
  
⚡️  **State Based Logic:**  Every state is handled by the State Controller and in it's own script, this makes changing a states visuals or logic easy and understandable.  
  
⚡️  **Simple API:**  The API is very simple and easy to change, and everything is documented and explained in the documentation!  
  

✨**BONUS CONTENT**✨  

✨  **State Machine:**  An easy to understand state machine that handles all states within the game. Adding states is as simple as making a script!  
  
✨  **View Controller:**  Handle all your menus, popups and other UI needs using the View Controller!  
  
✨  **Pixel Perfect Outline Shader:**  Used in this demo for highlighting characters who's turn it is and for selecting attack targets. You can use this shader for all sorts of things and even in other projects!  
  
✨  **Sprite Text Component:**  Easy Sprite Text component to use custom sprites for fonts instead of using font files!  
  

⚠️**IMPORTANT**⚠️  

I plan on updating this asset with many more features. As such, I will be increasing the price as I see fit for the features in the pack. Purchase the pack now to get  **FREE**  updates!

